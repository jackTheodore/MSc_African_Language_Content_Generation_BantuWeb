from lxml import html
import requests
import wikipedia
import json
import nltk
import os
import re

# Make it work for Python 2+3 and with Unicode
import io

#getting a list of all south african related topics from wikipedia
url = 'http://en.wikipedia.org/wiki/Outline_of_South_Africa'
tree  = html.fromstring(requests.get(url).text)
south_african_outline = tree.cssselect('div.mw-parser-output')[0].cssselect('ul li a')
outline_names = [t.text_content() for t in south_african_outline]

data = []

def getArticles(outline_names):
    for x in xrange(1,3):
        
        try: 
            page = outline_names[x]
            ny = wikipedia.page(page)
            data.append({'title' : ny.title, 'content' : ny.content})  
        except:
            #incase this breaks on a nun existing link, pass on to the next index
            continue

    return data 
#cleaning text by removing the double equal signs in the text from wikipedia
def remove_text_inside_doube_equalsigns(text, eq_signs="=="):
    count = [0] * (len(eq_signs) // 2) # count open/close eq_signs
    saved_chars = []
    #remove all https links in text
    text = re.sub(r'^https?:\/\/.*[\r\n]*', '', text, flags=re.MULTILINE) 
    for character in text:
        for i, b in enumerate(eq_signs):
            if character == b: # found doube_equal signs
                kind, is_close = divmod(i, 2)
                count[kind] += (-1)**is_close # `+1`: open, `-1`: close
                if count[kind] < 0: # unbalanced doube_equal signs
                    count[kind] = 0  # keep it
                else:  # found doube_equal signs to remove
                    break
        else: # character is not a [balanced] doube_equal signs
            if not any(count): # outside eq_signs
                saved_chars.append(character)
    return ''.join(saved_chars)

#get complete list of articles and content from wikipedia
result = getArticles(outline_names)

#clean_result =  
size = len(result)
print result[0]['title']

#print len(result) #result.keys()[0] + result.values()[0]

# for index in range (min (size,size)):
    
#     title = result.keys()[index]
#     content =  result.values()[index]
#     sents = nltk.sent_tokenize(content)
#     for index_number in range (min (len(sents), len(sents))):
        
#         clean_text = remove_text_inside_doube_equalsigns(sents[index_number])

#         trace = "["+str(index)+"] "+title+" [" +str(index_number)+ "]  " + clean_text
#         if len(sents[index_number]) != 0:
#             #print("------Article -----")
#             print(trace)
#             #  print("-------------------\n")




