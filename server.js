var express = require('express'),
    path = require('path');
//var logger = require('logger');
//var engines = require('consolidate');
//create our express app
var app = express();

//app.set("view options", {layout: false});
//app.use(express.static(__dirname + '/views'));
//app.set('views', __dirname + '/views');

//add some standard express middleware
//   app.configure(function() {
  //  app.use(logger('dev')); /* 'default', 'short', 'tiny', 'dev' */
   //app.use(express.bodyParser());
    //app.use(express.cookieParser());
    //app.use(express.static('static'));
//});

var http = require("http");

var options = {
  host:'localhost',
  port:8983,
  path:'/solr/demo/select?q=*chocolate*&wt=json&indent=on'
};

//http.createServer(function (request, response) {
app.get('/',function(request,response){
  response.writeHead(200, {'Content-Type': 'text/html'});

var req = http.request(options, function(res) {
  console.log('STATUS: ' + res.statusCode);
  console.log('HEADERS: ' + JSON.stringify(res.headers));
  res.setEncoding('utf8');
  res.on('data', function (chunk) {
    response.write(chunk);
    //console.log('BODY: ' + chunk["responseHeader"]);
  });
});

req.on('error', function(e) {
  console.log('problem with request: ' + e.message);
});

req.end();

});//.listen(8081);


/*app.get('/test', function(req, res) {
    app.engine('html', engines.mustache);
    res.render('index.html');
});*/

app.listen(8081)

console.log('Server running at http:/127.0.0.1:8081/');


