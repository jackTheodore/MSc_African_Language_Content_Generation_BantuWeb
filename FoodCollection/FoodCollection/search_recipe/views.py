# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from rest_framework.decorators import api_view
from urllib2 import *
from django.http import JsonResponse

@api_view(['GET', ])
def RecipeCollection(request):
	connection = urlopen('http://localhost:8983/solr/demo/select?indent=on&q=chocolate&wt=json')
	response = eval(connection.read())
	return JsonResponse(response)