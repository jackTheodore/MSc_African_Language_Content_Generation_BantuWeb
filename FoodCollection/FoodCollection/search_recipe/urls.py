from django.conf.urls import url
from .api import RecipeCollection

urlpatterns = [
     url(r'^recipes$', RecipeCollection.as_view()),
]