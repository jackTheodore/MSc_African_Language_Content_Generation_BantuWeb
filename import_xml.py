import xml.dom.minidom

def main():
	# use the parse() function to load nad parse an xml file
	doc = xml.dom.minidom.parse("sample.xml")

    # print  out the document node and the name of the first child tag
	print doc.nodeName
	print doc.firstChild.tagName

    #get  a list of xml tags from the document and print each one
	skills = getElementByTagName("monaco")
	print skills

    #create a new Xml tag and add it into the document
	#newSkill = doc.createElement("skill")
	#newSkill.setAttribute("name","jQuery")
	#doc.firstChild.appendChild(newSkill)

if __name__ == '__main__':
	main()
